#!/usr/bin/python
import argparse
import requests
import re
import sys
import subprocess
from urllib.parse import urlparse

def find_bitbucket_repo():
    """Find bitbucket repo."""
    cmd = ['git', 'config', '--get', 'remote.origin.url']
    url = subprocess.check_output(cmd, universal_newlines=True).strip()
    o = urlparse(url)
    if o.scheme == '':
        o = urlparse("ssh://" + url.replace(":", "/"))

    if o.hostname != "bitbucket.org":
        raise ValueError("no bitbucket repo found")

    return o

def get_auth(repo):
    """Get credentials git."""
    p = subprocess.Popen(['git', 'credential','fill'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    msg = "protocol={protocol}\nhost={host}\npath={path}\n\n".format(protocol=repo.scheme, host=repo.hostname, path=repo.path)
    answer = p.communicate(input=msg.encode('ascii'))[0].decode().strip()
    answer = dict(x.split('=') for x in answer.split('\n'))

    def cb(approve=False):
        command = approve and 'approve' or 'reject'
        p = subprocess.Popen(['git', 'credential', command], stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
        msg = "protocol={protocol}\nhost={host}\npath={path}\nusername={username}\npassword={password}\n\n".format(**answer)
        p.communicate(input=msg.encode('ascii'))[0].decode().strip()

    return (answer["username"], answer["password"]), cb

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

class Bitbucket(object):
    def __init__(self, repo=None, needs_auth=False):
        if repo is None:
            repo = find_bitbucket_repo()
        self.uri = repo

        remove = ".git"
        if repo.path.endswith(remove):
            self.repo = repo.path[:-len(remove)]
        else:
            self.repo = repo.path
        self.repo = self.repo.lstrip("/")

        self.auth = None

        bb_url = self.issue_api_url()
        repo_status = requests.head(bb_url).status_code

        if repo_status == 404:
            raise RuntimeError( "Could not find a Bitbucket Issue Tracker at: {}\n" .format(bb_url))
        elif repo_status == 403 or needs_auth:
            self.auth, callback = get_auth(repo)
            repo_status = requests.head(bb_url, auth=self.auth).status_code
            if repo_status != 200:
                callback(approve=False)
                raise RuntimeError( "Authentication failure for Bitbucket Issue Tracker at: {}\n" .format(bb_url))
            else:
                callback(approve=True)

    def repo_name(self):
        return self.repo

    def issue_api_url(self, issue_id=None, comments=False):
        if issue_id is None:
            return "https://api.bitbucket.org/1.0/repositories/{repo}/issues".format(repo=self.repo)
        elif not comments:
            return "https://api.bitbucket.org/1.0/repositories/{repo}/issues/{id}".format(repo=self.repo, id=issue_id)
        else:
            return "https://api.bitbucket.org/1.0/repositories/{repo}/issues/{id}/comments/".format(repo=self.repo, id=issue_id)

    def repo_url(self):
        return "https://bitbucket.org/{repo}/".format(repo=self.repo)

    def issue_url(self, issue_id=None):
        if issue_id is None:
            return "https://bitbucket.org/{repo}/issues".format(repo=self.repo)
        else:
            return "https://bitbucket.org/{repo}/issues/{id}".format(repo=self.repo, id=issue_id)

    def get_issues(self, offset=0):
        """Fetch the issues from Bitbucket."""
        issues = []
        initial_offset = offset

        while True:  # keep fetching additional pages of issues until all processed
            respo = requests.get(
                self.issue_api_url(), auth=self.auth,
                params={'sort': 'local_id', 'start': offset, 'limit': 50}
            )
            if respo.status_code == 200:
                result = respo.json()
                # check to see if there are issues to process, if not break out.
                if not result['issues']:
                    break
                issues += result['issues']
                # 'start' is the current list index of the issue, not the issue ID
                offset += len(result['issues'])
            else:
                raise RuntimeError(
                    "Bitbucket returned an unexpected HTTP status code: {}"
                    .format(respo.status_code)
                )

        assert len(issues) == result['count'] - initial_offset
        return issues

    def get_issue(self, issue_id):
        respo = requests.get(self.issue_api_url(issue_id), auth=self.auth)
        if respo.status_code != 200:
            raise RuntimeError(
                "Failed to get issue from: {} due to unexpected HTTP "
                "status code: {}"
                .format(self.issue_api_url(issue_id), respo.status_code)
            )

        return respo.json()

    def get_issue_comments(self, issue_id):
        """Fetch the comments for the specified Bitbucket issue."""
        respo = requests.get(self.issue_api_url(issue_id, True), auth=self.auth)
        if respo.status_code != 200:
            raise RuntimeError(
                "Failed to get issue comments from: {} due to unexpected HTTP "
                "status code: {}"
                .format(self.issue_api_url(issue_id, True), respo.status_code)
            )
        return respo.json()

    def create_issue(self, title, content, **kwargs):
        """
            Attributes are:
            * title: The title of the new issue.
            * content: The content of the new issue.
            * component: The component associated with the issue.
            * milestone: The milestone associated with the issue.
            * version: The version associated with the issue.
            * responsible: The username of the person responsible for the issue.
            * status: The status of the issue (new, open, resolved, on hold, invalid, duplicate, or wontfix).
            * kind: The kind of issue (bug, enhancement, or proposal).
        """
        data = { 'title' : title, 'content' : content }
        data.update(kwargs)

        respo = requests.post(self.issue_api_url(), data, auth=self.auth)
        if respo.status_code != 200:
            raise RuntimeError(
                "Failed to create issue from: {} due to unexpected HTTP "
                "status code: {}"
                .format(self.issue_api_url(), respo.status_code)
            )

        return respo.json()

    def update_issue(self, issue_id, **kwargs):
        """
            Attributes are:
            * title: The title of the new issue.
            * content: The content of the new issue.
            * component: The component associated with the issue.
            * milestone: The milestone associated with the issue.
            * version: The version associated with the issue.
            * responsible: The username of the person responsible for the issue.
            * status: The status of the issue (new, open, resolved, on hold, invalid, duplicate, or wontfix).
            * kind: The kind of issue (bug, enhancement, or proposal).
        """
        respo = requests.put( self.issue_api_url(issue_id), kwargs, auth=self.auth)
        if respo.status_code != 200:
            raise RuntimeError(
                "Failed to update issue from: {} due to unexpected HTTP "
                "status code: {}"
                .format(self.issue_api_url(issue_id), respo.status_code)
            )

        return respo.json()

    def delete_issue(self, issue_id):
        respo = requests.delete(self.issue_api_url(issue_id), auth=self.auth)
        if respo.status_code != 200 and respo.status_code != 204:
            raise RuntimeError(
                "Failed to delete issue from: {} due to unexpected HTTP "
                "status code: {}"
                .format(self.issue_api_url(issue_id), respo.status_code)
            )

def format_user(user, nouser_name="Anonymous"):
    if user is None:
        return nouser_name
    else:
        return user['username']

def format_issue_body(issue, url):
    content = issue['content']
    return """{id}: {title}
Reported by: **{reporter}**
Assignee: {assignee}
Status: {status}
Priority: {priority}
{sep}
{content}
{sep}
- Bitbucket: {url}/{id}
""".format(title=issue['title'],
        status=issue['status'],
        priority=issue['priority'],
        # anonymous issues are missing 'reported_by' key
        reporter=format_user(issue.get('reported_by', None)),
        assignee=format_user(issue.get('responsible', None), "none"),
        sep='-' * 40,
        content=content,
        url=url,
        id=issue['local_id'],)

def format_comment_body(comment):
    content = comment['content']
    return """Comment by **{author}**:
{sep}
{content}
""".format(
        author=format_user(comment['author_info']),
        sep='-' * 40,
        content=content)


def create_filter(x):
    k,v = x.split('=')
    if v[0] == '-':
        v = re.compile(v[1:])
        return (k, lambda y: not v.match(y))
    else:
        v = re.compile(v)
        return (k, lambda y: v.match(y))

def cmd_list(bb, options):
    filters = options.filter
    if filters:
        filters = dict(create_filter(x) for x in filters.split(','))

    def test_issue(issue):
        if not filters:
            return True

        for k,filt in filters.items():
            if not k in issue:
                continue

            if not filt(issue[k]):
                return False

        return True

    issues = bb.get_issues()
    for index, issue in enumerate(issues):
        if test_issue(issue):
            print( format_issue_body(issue, bb.issue_url()) )

def cmd_show(bb, options):
    issue = bb.get_issue(options.issue_id)
    print( format_issue_body(issue, bb.issue_url()) )

    comments = bb.get_issue_comments(options.issue_id)
    for c in comments:
        if c['content']:
            print( format_comment_body(c) )

def cmd_create(bb, options):
    issue = bb.create_issue(title=options.title, content=options.content,
                component=options.component, milestone=options.milestone,
                version=options.version, responsible=options.responsible,
                status=options.status, kind=options.kind, priority=options.priority)
    print( format_issue_body(issue, bb.issue_url()) )

def cmd_delete(bb, options):
    issue = bb.get_issue(options.issue_id)
    print( format_issue_body(issue, bb.issue_url()) )
    answer = options.yes
    if not answer:
        answer = query_yes_no("Sure you want to delete this issue?", default="no")

    if answer:
        bb.delete_issue(options.issue_id)

    return answer and 0 or 1

def cmd_update(bb, options):
    legal_fields = ["title", "content", "component", "milestone", "version", "responsible", "status", "kind", "priority"]
    kwargs = { k : getattr(options, k) for k in legal_fields }

    issue = bb.update_issue(options.issue_id, **kwargs)
    print( format_issue_body(issue, bb.issue_url()) )

def main(options):
    method_to_call = options.func
    needs_auth = method_to_call != cmd_list and method_to_call != cmd_show

    bb = Bitbucket(needs_auth=needs_auth)
    return method_to_call(bb, options)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Bitbucket issues")
    subparsers = parser.add_subparsers(title='subcommands', dest="cmd", help='sub-command help')
    subparsers.required = True

    parser_list = subparsers.add_parser('list', help='list help')
    parser_list.add_argument("--filter", type=str, help="Filter issues", default=None)
    parser_list.set_defaults(func=cmd_list)

    parser_show = subparsers.add_parser('show', description="Show issue from bitbucket", help="show help")
    parser_show.add_argument("issue_id", type=int, help="Which issue to show")
    parser_show.set_defaults(func=cmd_show)

    parser_create = subparsers.add_parser('create', description="Create issue on bitbucket", help="create help")
    parser_create.add_argument("--title", type=str, help="The title of the new issue", required=True)
    parser_create.add_argument("--content", type=str, help="The content of the new issue", required=True)
    parser_create.add_argument("--component", type=str, help="The component associated with the issue", default=None)
    parser_create.add_argument("--milestone", type=str, help="The milestone associated with the issue", default=None)
    parser_create.add_argument("--version", type=str, help="The version associated with the issue", default=None)
    parser_create.add_argument("--responsible", type=str, help="The username of the person responsible for the issue", default=None)
    parser_create.add_argument("--status", type=str, help="The status of the issue (new, open, resolved, on hold, invalid, duplicate, or wontfix)", default="new")
    parser_create.add_argument("--kind", type=str, help="The kind of issue (bug, enhancement, or proposal)", default="bug")
    parser_create.add_argument("--priority", type=str, help="The priority of issue (minor, major, critical, blocking or trivial)", default="minor")
    parser_create.set_defaults(func=cmd_create)

    parser_delete = subparsers.add_parser('delete', description="Delete issue from bitbucket", help="delete help")
    parser_delete.add_argument("issue_id", type=int, help="Which issue to delete")
    parser_delete.add_argument("--yes", action='store_true', help="Answer yes on questions")
    parser_delete.set_defaults(func=cmd_delete)

    parser_update = subparsers.add_parser('update', description="Update issue on bitbucket", help="update help")
    parser_update.add_argument("issue_id", type=int, help="Which issue to update")
    parser_update.add_argument("--title", type=str, help="The title of the new issue", default=None)
    parser_update.add_argument("--content", type=str, help="The content of the new issue", default=None)
    parser_update.add_argument("--component", type=str, help="The component associated with the issue", default=None)
    parser_update.add_argument("--milestone", type=str, help="The milestone associated with the issue", default=None)
    parser_update.add_argument("--version", type=str, help="The version associated with the issue", default=None)
    parser_update.add_argument("--responsible", type=str, help="The username of the person responsible for the issue", default=None)
    parser_update.add_argument("--status", type=str, help="The status of the issue (new, open, resolved, on hold, invalid, duplicate, or wontfix)", default=None)
    parser_update.add_argument("--kind", type=str, help="The kind of issue (bug, enhancement, or proposal)", default=None)
    parser_update.add_argument("--priority", type=str, help="The priority of issue (minor, major, critical, blocking or trivial)", default="minor")
    parser_update.set_defaults(func=cmd_update)

    options = parser.parse_args()
    try:
        ret = main(options)
        sys.exit( ret )
    except RuntimeError as e:
        print(e, file=sys.stderr)
